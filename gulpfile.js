'use strict';

var path = require('path'),
    gulp = require('gulp'),
    util = require('gulp-util'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify'),
    changed      = require('gulp-changed'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    watch = require('gulp-watch');

var bowerComponentsFolder = 'bower_components';
var assetsFolder          = 'assets';

gulp.task('default', function(){
    gulp.run('scripts');
    gulp.run('sass');
    gulp.run('watch');
});

// ### Fonts
// `gulp fonts` - Grabs all the fonts and outputs them in a flattened directory
// structure. See: https://github.com/armed/gulp-flatten
gulp.task('fonts', function() {
    return gulp.src(globs.fonts)
        .pipe(flatten())
        .pipe(gulp.dest(path.dist + 'fonts'))
        .pipe(browserSync.stream());
});

// ### Images
// `gulp images` - Run lossless compression on all the images.
gulp.task('images', function() {
    return gulp.src(globs.images)
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
        }))
        .pipe(gulp.dest(path.dist + 'images'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function(){
    return gulp.src([
            bowerComponentsFolder + '/jquery/dist/jquery.min.js',
            bowerComponentsFolder + '/jquery.easing/js/jquery.easing.min.js',
            bowerComponentsFolder + '/bootstrap/dist/js/bootstrap.min.js',
            // O arquivo .js da aplicacaoo
            assetsFolder + '/js/app.js'
        ])
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(assetsFolder + '/js'));
});

gulp.task('sass', function(){
    // O arquivo less principal
    return gulp.src(assetsFolder + '/scss/main.scss')
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(gulp.dest(assetsFolder + '/css'));
});

// ### Wiredep
// `gulp wiredep` - Automatically inject Less and Sass Bower dependencies. See
// https://github.com/taptapship/wiredep
gulp.task('wiredep', function() {
    var wiredep = require('wiredep').stream;
    return gulp.src(assetsFolder + '/scss/main.scss')
        .pipe(wiredep())
        .pipe(changed(assetsFolder + '/scss', {
            hasChanged: changed.compareSha1Digest
        }))
        .pipe(gulp.dest(assetsFolder + '/scss'));
});

gulp.task('watch', function(){
    gulp.watch(assetsFolder + '/scss/**/*.scss', ['sass']);
    gulp.watch(assetsFolder + '/js/*.js', ['scripts']);
});